<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/testtask/interfaces/IDev.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/testtask/classes/Employer.php';

class Dev extends Employer implements IDev, SplSubject
{
    private $observers;
    public $workResult;

    public function __construct($name)
    {
        parent::__construct($name);
        $this->observers = new SplObjectStorage;
    }

    public function getWorkResult(): bool
    {
        if (empty($this->workResult)) {
            throw new Exception('Work result not available');
        } else {
            return $this->workResult;
        }
    }
    public function setWorkResult(bool $result)
    {
        $this->workResult = $result;

        echo 'DEV: Hey, Lead, look whad i did: (result) = ' . ($result ? 'success job' : 'failed job');
        echo "<br/>";
        $this->notify();
    }

    public function attach(SplObserver $observer)
    {
        $this->observers->attach($observer);
    }

    public function detach(SplObserver $observer)
    {
        $this->observers->detach($observer);
    }

    public function notify()
    {
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }
}
