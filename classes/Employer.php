<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/testtask/interfaces/IEmployer.php';

class Employer implements IEmployer
{
    private $name = '';

    public function __construct($name)
    {
        $this->setName($name);
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
