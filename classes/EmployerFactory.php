<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/testtask/interfaces/IEmployerFactory.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/testtask/classes/Dev.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/testtask/classes/TechLead.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/testtask/classes/HR.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/testtask/classes/Manager.php';

class EmployerFactory implements IEmployerFactory
{
    public function createDevEmployer(string $name): IDev
    {
        return new Dev($name);
    }
    public function createTechLeadEmployer(string $name): ITechLead
    {
        return new TechLead($name);
    }
    public function createHREmployer(string $name): IHR
    {
        return new HR($name);
    }
    public function createManagerEmployer(string $name): IManager
    {
        return new Manager($name);
    }
}
