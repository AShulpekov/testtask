<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/testtask/interfaces/IHR.php';

class HR extends Employer implements IHR, SplObserver
{
    private $mood;
    private $failedDevResultCounter = 0;

    public function getMood()
    {
        return $this->mood;
    }
    public function setMoodFromState(int $mood)
    {
        $this->mood = $mood;
    }
    public function observeForDev(IDev $dev)
    {
    }

    public function update(SplSubject $subject)
    {
        $this->checkTechLeadMood($subject->mood);
    }


    private function checkTechLeadMood($mood)
    {
        if ($mood <= 1) {
            $this->addFailedDevResults();
        }
    }

    private function addFailedDevResults()
    {
        $this->failedDevResultCounter++;
    }

    public function getFailedDevResult()
    {
        return $this->failedDevResultCounter;
    }
}
