<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/testtask/interfaces/IManager.php';

class Manager extends Employer implements IManager, SplObserver
{
    private $mood;
    private $successResultCounter = 0;
    public function getMood()
    {
        return $this->mood;
    }
    public function setMoodFromState(int $mood)
    {
        $this->mood = $mood;
    }

    private function checkTechLeadMood($mood)
    {
        if ($mood >= 4) {
            $this->addSuccessDevResults();
        }
    }

    private function addSuccessDevResults()
    {
        $this->successResultCounter++;
    }

    public function getSuccessDevResult()
    {
        return $this->successResultCounter;
    }


    public function update(SplSubject $subject)
    {
        $this->checkTechLeadMood($subject->mood);
    }
}
