<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/testtask/interfaces/ITechLead.php';

class TechLead extends Employer implements ITechLead, SplObserver, SplSubject
{
    public $mood = 4;
    private $moodDescriptions = [
        1 => 'I hate everything, especially you, i think about leave this place and become a farmer!!! >_<',
        2 => 'Omg, what are you doing??? i have a bad mood! 0_0',
        3 => 'Nothing special, i have a normal mood.',
        4 => 'Good job, i have a good mood! ^_^'
    ];

    public function __construct($name)
    {
        parent::__construct($name);
        $this->observers = new SplObjectStorage;
    }

    public function getMood()
    {
        return $this->mood;
    }

    public function setMood(int $mood)
    {
        $this->mood = $mood;
    }

    public function changeMood(bool $result)
    {
        if ($result) {
            $this->mood = $this->mood >= $this->getBestMoodIndex() ? $this->getBestMoodIndex() : ++$this->mood;
        } else {
            $this->mood = $this->mood === 1 ? 1 : --$this->mood;
        }

        echo 'TECHLEAD: ' . $this->moodDescriptions[$this->mood];
        echo "<br/>";

        $this->notify();
    }

    public function getBestMoodIndex()
    {
        return count($this->moodDescriptions);
    }

    public function getWorstMood()
    {
        return min(array_keys($this->moodDescriptions));
    }

    public function update(SplSubject $subject)
    {
        $this->changeMood($subject->workResult);
    }

    public function attach(SplObserver $observer)
    {
        $this->observers->attach($observer);
    }

    public function detach(SplObserver $observer)
    {
        $this->observers->detach($observer);
    }

    public function notify()
    {
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }
}
