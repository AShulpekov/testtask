<?php

/**
 * This code for sure gonna work on PHP 7.0
 */
require_once "./classes/EmployerFactory.php";
try {
    $employerFactory = new EmployerFactory();

    $developer = $employerFactory->createDevEmployer('Developer');
    $techLead = $employerFactory->createTechLeadEmployer('TechLead(T-70)');
    $hr = $employerFactory->createHREmployer('HR(T-1000)');
    $junHr = $employerFactory->createHREmployer('JunHR(T-100)');
    $manager = $employerFactory->createManagerEmployer('Manager(T-1001)');

    echo "----------------------------------------------------------<br/>";
    echo 'Developer created with name: ' . $developer->getName();
    echo "<br/>";
    echo 'HR created with name: ' . $hr->getName();
    echo "<br/>";
    echo 'Manager created with name: ' . $manager->getName();
    echo "<br/>";
    echo 'techLead created with name: ' . $techLead->getName();
    echo "<br/>";
    echo "----------------------------------------------------------<br/>";

    $developer->attach($techLead);
    echo 'TechLead become observe for Dev';
    echo "<br/>";
    $techLead->attach($hr);
    echo 'HR become observe for TechLead';
    echo "<br/>";
    $techLead->attach($junHr);
    echo 'JunHR become observe for TechLead';
    echo "<br/>";
    $techLead->attach($manager);
    echo 'Manager become observe for TechLead';
    echo "<br/>";
    echo "----------------------------------------------------------<br/>";


    $developer->setWorkResult(false);
    echo "<br/>";
    $developer->setWorkResult(true);
    echo "<br/>";
    $developer->setWorkResult(true);
    echo "<br/>";
    $developer->setWorkResult(false);
    echo "<br/>";
    $developer->setWorkResult(false);
    echo "<br/>";
    $developer->setWorkResult(false);
    echo "<br/>";
    $developer->setWorkResult(false);
    echo "<br/>";
    $developer->setWorkResult(true);
    echo "<br/>";
    $developer->setWorkResult(true);
    echo "<br/>";
    $developer->setWorkResult(true);
    echo "<br/>";

    echo "----------------------------------------------------------<br/>";
    echo "1 month later...<br/>";
    echo "Manager reporting: Developer made Techlead happy " . $manager->getSuccessDevResult() . " times";
    echo "<br/>";
    echo "HR reporting: Developer made Techlead mad " . $hr->getFailedDevResult() . " times";
    echo "<br/>";
    echo "JunHR reporting: Developer made Techlead mad " . $junHr->getFailedDevResult() . " times";
} catch (Exception $e) {
    echo $e->getMessage();
}
