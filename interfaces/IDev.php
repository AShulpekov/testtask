<?php

interface IDev
{
    public function getWorkResult(): bool;
    public function setWorkResult(bool $result);
}
