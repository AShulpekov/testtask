<?php

interface IEmployer
{
    public function getName(): string;
    public function setName(string $name);
}
