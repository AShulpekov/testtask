<?php

interface IEmployerFactory
{
    public function createDevEmployer(string $name): IDev;
    public function createTechLeadEmployer(string $name): ITechLead;
    public function createHREmployer(string $name): IHR;
    public function createManagerEmployer(string $name): IManager;
}
