<?php 

interface IHR {
    public function getMood();
    public function setMoodFromState(int $mood);
    public function observeForDev(IDev $dev);
}