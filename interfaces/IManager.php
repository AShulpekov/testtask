<?php 

interface IManager {
    public function getMood();
    public function setMoodFromState(int $mood);
}