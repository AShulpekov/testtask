<?php

interface ITechLead
{
    public function getMood();
    public function setMood(int $mood);
    public function changeMood(bool $mood);
}
